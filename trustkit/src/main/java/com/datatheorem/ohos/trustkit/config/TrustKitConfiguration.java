/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.datatheorem.ohos.trustkit.config;


import ohos.app.Context;

import org.dom4j.Document;

import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.HashSet;
import java.util.Set;

public class TrustKitConfiguration {

    private final Set<DomainPinningPolicy> domainPolicies;

    private final boolean shouldOverridePins;
    private final Set<Certificate> debugCaCertificates;


    public static TrustKitConfiguration fromXmlPolicy(
        Context context, Document parser
    ) throws CertificateException, IOException {
        return TrustKitConfigurationParser.fromXmlPolicy(context, parser);
    }


    protected TrustKitConfiguration(Set<DomainPinningPolicy> domainConfigSet) {
        this(domainConfigSet, false, null);
    }

    protected TrustKitConfiguration(
        Set<DomainPinningPolicy> domainConfigSet,
        boolean shouldOverridePins,
        Set<Certificate> debugCaCerts
    ) {
        Set<String> hostnameSet = new HashSet<>();
        for (DomainPinningPolicy domainConfig : domainConfigSet) {
            if (hostnameSet.contains(domainConfig.getHostname())) {
                throw new ConfigurationException("Policy contains the same domain defined twice: "
                    + domainConfig.getHostname());
            }
            hostnameSet.add(domainConfig.getHostname());
        }
        this.domainPolicies = domainConfigSet;
        this.shouldOverridePins = shouldOverridePins;
        this.debugCaCertificates = debugCaCerts;
    }

    public boolean shouldOverridePins() {
        return shouldOverridePins;
    }

    public Set<Certificate> getDebugCaCertificates() {
        return debugCaCertificates;
    }

    /**
     * Get the Set of {@link DomainPinningPolicy}.
     *
     * @return Set<DomainPinningPolicy> the set of domain's policy
     */
    public Set<DomainPinningPolicy> getAllPolicies() {
        return this.domainPolicies;
    }

    /**
     * Get the {@link DomainPinningPolicy} corresponding to the provided hostname.
     * When matching the most specific matching domain rule will be used, if no match exists
     * then null will be returned.
     *
     * @param serverHostname the server's hostname
     * @return DomainPinningPolicy the domain's policy or null if the supplied hostname has no
     * policy defined
     */
    public DomainPinningPolicy getPolicyForHostname(String serverHostname) {
        // Check if the hostname seems valid
        DomainValidator domainValidator = DomainValidator.getInstance(true);

        if (!domainValidator.isValid(serverHostname)) {
            throw new IllegalArgumentException("Invalid domain supplied: " + serverHostname);
        }

        DomainPinningPolicy bestMatchPolicy = null;
        for (DomainPinningPolicy domainPolicy : this.domainPolicies) {
            if (domainPolicy.getHostname().equals(serverHostname)) {
                // Found an exact match for this domain
                bestMatchPolicy = domainPolicy;
                break;
            }

            // Look for the best match for pinning policies that include subdomains
            if (domainPolicy.shouldIncludeSubdomains()
                && isSubdomain(domainPolicy.getHostname(), serverHostname)) {
                if (bestMatchPolicy == null) {
                    bestMatchPolicy = domainPolicy;
                } else if (domainPolicy.getHostname().length() > bestMatchPolicy.getHostname().length()) {
                    bestMatchPolicy = domainPolicy;
                }
            }
        }
        return bestMatchPolicy;
    }

    /**
     * Return true for all subdomains, including subdomains of subdomains, similar to how
     *
     * @param domain
     * @param subdomain
     * @return subdomain.endsWith(domain) && subdomain.charAt(subdomain.length() - domain.length() - 1) == '.';
     */
    private static boolean isSubdomain(String domain, String subdomain) {
        return subdomain.endsWith(domain)
            && subdomain.charAt(subdomain.length() - domain.length() - 1) == '.';
    }
}
