/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.datatheorem.ohos.trustkit.utils;

import ohos.agp.render.render3d.BuildConfig;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * TrustKitLog日志打印工具类
 *
 * @since 2021-03-22
 */
public final class TrustKitLog {
    // 定义日志标签
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "TrustKit");

    private TrustKitLog() {
    }

    /**
     * debug需要打印的文字
     *
     * @param message
     **/
    public static void i(String message) {
        // Disable debug printing for Release build
        if (BuildConfig.DEBUG) {
            HiLog.info(LABEL, message);
        }
    }

    /**
     * 需要打印的文字
     *
     * @param message
     **/
    public static void w(String message) {
        HiLog.info(LABEL, message);
    }
}
