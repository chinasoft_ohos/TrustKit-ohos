/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.datatheorem.ohos.trustkit.pinning;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * {@link Interceptor} used to parse the hostname of the {@link Request} URL and then save the
 * hostname in the {@link OkHttpRootTrustManager} which will later be used for Certificate Pinning.
 */
public class OkHttp2PinningInterceptor implements Interceptor {
    private final OkHttpRootTrustManager mTrustManager;

    public OkHttp2PinningInterceptor( OkHttpRootTrustManager trustManager) {
        mTrustManager = trustManager;
    }

    @Override public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String serverHostname = request.url().getHost();

        mTrustManager.setServerHostname(serverHostname);
        return chain.proceed(request);
    }
}
