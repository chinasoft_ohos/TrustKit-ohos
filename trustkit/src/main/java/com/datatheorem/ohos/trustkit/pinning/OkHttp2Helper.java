/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.datatheorem.ohos.trustkit.pinning;


import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

public class OkHttp2Helper {
    private static X509TrustManager trustManager;


    static {
        trustManager = new OkHttpRootTrustManager();
    }

    /**
     * Retrieve an {@code SSLSSocketFactory} that implements SSL pinning validation based on the
     * current TrustKit configuration. It can be used with an OkHttpClient to add SSL
     * pinning validation to the connections.
     *
     * <p>
     * The {@code SSLSocketFactory} is configured for the current TrustKit configuration and
     * will enforce the configuration's pinning policy.
     * </p>
     * @return SSLSocketFactory
     * @throws IllegalStateException
     */
    public static SSLSocketFactory getSSLSocketFactory() {
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new X509TrustManager[]{trustManager}, null);

            return sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
            throw new IllegalStateException("SSLSocketFactory creation failed");
        }
    }

    /**
     * Returns an {@link com.squareup.okhttp.Interceptor} used to parse the hostname of the
     * {@link Request} URL and then save the hostname in the {@link OkHttpRootTrustManager} which will
     * later be used for Certificate Pinning.
     * @return Interceptor
     */
    public static Interceptor getPinningInterceptor() {
        return new OkHttp2PinningInterceptor((OkHttpRootTrustManager)trustManager);
    }
}

