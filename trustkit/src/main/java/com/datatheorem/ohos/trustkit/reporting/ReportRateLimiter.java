/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.datatheorem.ohos.trustkit.reporting;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// Very basic implementation to rate-limit identical reports to once a day
class ReportRateLimiter {

    private static final long MAX_SECONDS_BETWEEN_CACHE_RESET = 3600*24;
    private static final Set<List<Object>> reportsCache = new HashSet<>();
    protected static Date lastReportsCacheResetDate = new Date();

    synchronized static boolean shouldRateLimit( final PinningFailureReport report) {
        // Reset the cache if it was created more than 24 hours ago
        Date currentDate = new Date();
        long secondsSinceLastReset =
                (currentDate.getTime() / 1000) - (lastReportsCacheResetDate.getTime() / 1000);
        if (secondsSinceLastReset > MAX_SECONDS_BETWEEN_CACHE_RESET) {
            reportsCache.clear();
            lastReportsCacheResetDate = currentDate;
        }

        // Check to see if an identical report is already in the cache
        List<Object> cacheEntry = new ArrayList<>();
        cacheEntry.add(report.getNotedHostname());
        cacheEntry.add(report.getServerHostname());
        cacheEntry.add(report.getServerPort());
        cacheEntry.add(report.getValidatedCertificateChainAsPem());
        cacheEntry.add(report.getValidationResult());

        boolean shouldRateLimitReport = reportsCache.contains(cacheEntry);
        if (!shouldRateLimitReport){
            reportsCache.add(cacheEntry);
        }
        return shouldRateLimitReport;
    }
}

