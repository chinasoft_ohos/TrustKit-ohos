/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.datatheorem.ohos.trustkit.asyntaskutils;

/**
 * 线程type类型枚举
 *
 * @since 2021-03-19
 */
public enum TaskDispatcherEnum {
    /**
     * 全局并发任务分发器TYPE
     **/
    GLOBAL,
    /**
     * 并发任务分发器TYPE
     **/
    PARALLEL,
    /**
     * 串行任务分发器TYPE
     **/
    SERIAL,
    /**
     * 专有任务分发器-MAIN
     **/
    MAIN,
    /**
     * 专有任务分发器-UI
     **/
    UI;

    TaskDispatcherEnum() {
    }
}
