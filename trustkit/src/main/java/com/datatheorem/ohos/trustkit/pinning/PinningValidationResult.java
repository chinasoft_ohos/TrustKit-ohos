/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.datatheorem.ohos.trustkit.pinning;


public enum PinningValidationResult {
    // The server trust was successfully evaluated and contained at least one of the configured pins
    SUCCESS,

    // The server trust was successfully evaluated but did not contain any of the configured pins
    FAILED,

    // The server trust's evaluation failed: the server's certificate chain is not trusted
    FAILED_CERTIFICATE_CHAIN_NOT_TRUSTED,

    ERROR_INVALID_PARAMETERS,

    FAILED_USER_DEFINED_TRUST_ANCHOR,

    ERROR_COULD_NOT_GENERATE_SPKI_HASH
}
