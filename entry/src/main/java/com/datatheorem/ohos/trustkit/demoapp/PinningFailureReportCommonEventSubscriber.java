/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.datatheorem.ohos.trustkit.demoapp;

import static com.datatheorem.ohos.trustkit.demoapp.slice.DemoMainAbilitySlice.DEBUG_TAG;

import ohos.event.commonevent.CommonEventData;
import ohos.event.commonevent.CommonEventSubscribeInfo;
import ohos.event.commonevent.CommonEventSubscriber;
import ohos.hiviewdfx.HiLog;

/**
 * Class that provides an example broadcast receiver
 *
 * <p>
 * Applications using TrustKit can listen for local broadcasts and receive the same report that
 * would be sent to the report_url.
 * </p>
 **/
public class PinningFailureReportCommonEventSubscriber extends CommonEventSubscriber {
    public PinningFailureReportCommonEventSubscriber(CommonEventSubscribeInfo info) {
        super(info);
    }

    @Override
    public void onReceiveEvent(CommonEventData commonEventData) {
        HiLog.info(DEBUG_TAG, commonEventData.getData());
    }
}
