# TrustKit-ohos
#### 项目介绍
- 项目名称：TrustKit-ohos
- 所属系列：openharmony第三方组件适配移植
- 功能：提供在任何Ohos应用程序中轻松部署ssl公钥锁定和报告功能的库
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 1.1.3

#### 效果演示

![TrustKit-ohos](/screenshots/trustkit.gif)

#### 安装教程

1.在项目根目录下的build.gradle文件中，

```JAVA
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
```
2.在entry模块的build.gradle文件中，
```JAVA
dependencies {
   implementation('com.gitee.chinasoft_ohos:trustkit:1.0.1')
   ......  
}
```
在sdk6，DevEco Studio2.2 beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下


#### 使用说明

**TrustKit-ohos** 是一个开源的库,可以很容易的在任何一个Ohos应用上布置SSL公钥锁和报告;

TrustKit应该用相同的路径初始化:

```java
@Override
public void onStart(Intent intent) {
    super.onStart(intent);

  // Using the default path - res/xml/network_security_config.xml
  TrustKit.initializeWithNetworkSecurityConfiguration(this);

  // OR using a custom resource (TrustKit can't be initialized twice)
  TrustKit.initializeWithNetworkSecurityConfiguration(this, R.xml.my_custom_network_security_config);

  URL url = new URL("https://www.datatheorem.com");
  String serverHostname = url.getHost();
  
  //Optionally add a local broadcast receiver to receive PinningFailureReports
  PinningValidationReportTestBroadcastReceiver receiver = new PinningValidationReportTestBroadcastReceiver();
          LocalBroadcastManager.getInstance(context)
                  .registerReceiver(receiver, new IntentFilter(BackgroundReporter.REPORT_VALIDATION_EVENT));

  // HttpsUrlConnection
  HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
  connection.setSSLSocketFactory(TrustKit.getInstance().getSSLSocketFactory(serverHostname));

  // OkHttp 2.x
  OkHttpClient client =
    new OkHttpClient()
        .setSslSocketFactory(OkHttp2Helper.getSSLSocketFactory());
  client.interceptors().add(OkHttp2Helper.getPinningInterceptor());
  client.setFollowRedirects(false);

  // OkHttp 3.0.x, 3.1.x and 3.2.x
  OkHttpClient client =
    new OkHttpClient.Builder()
        .sslSocketFactory(OkHttp3Helper.getSSLSocketFactory())
        .addInterceptor(OkHttp3Helper.getPinningInterceptor())
        .followRedirects(false)
        .followSslRedirects(false)

  // OkHttp 3.3.x and higher
  OkHttpClient client =
    new OkHttpClient.Builder()
        .sslSocketFactory(OkHttp3Helper.getSSLSocketFactory(), OkHttp3Helper.getTrustManager())
        .addInterceptor(OkHttp3Helper.getPinningInterceptor())
        .followRedirects(false)
        .followSslRedirects(false)
    .build();
}
public class PinningFailureReportCommonEventSubscriber extends CommonEventSubscriber {
    public PinningFailureReportCommonEventSubscriber(CommonEventSubscribeInfo info) {
        super(info);
    }

    @Override
    public void onReceiveEvent(CommonEventData commonEventData) {
        HiLog.info(DEBUG_TAG, commonEventData.getData());
    }
}
```

初始化TrustKit并设置客户端或连接的SSLSocketFactory之后，无论何时启动HTTPS连接，它都将根据配置的固定策略验证服务器的证书链。如果一个报告URI已经配置，当pin验证失败时，应用程序也将发送报告到指定的URI。

您还可以创建并注册本地广播接收器，以接收将发送到report_uri的相同证书固定错误报告。

#### 测试信息
CodeCheck代码测试无异常  
CloudTest代码测试无异常  
安全病毒安全检测通过  
当前版本demo功能与原组件基本无差异  

#### 版本迭代
- 1.0.1  
